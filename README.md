# maxicron-yum-check


PATH=/bin:/usr/bin:/sbin:/usr/sbin
export PATH

# Locations of binaries
GREP="/bin/grep"
HOST=`hostname`
MAIL="/bin/mail"
MKTEMP="/bin/mktemp"
YUM="/usr/bin/yum"

# Who to E-mail with new updates
ADMIN=webmaster@sofibox.com

if [ ! -f ${YUM} ];then
