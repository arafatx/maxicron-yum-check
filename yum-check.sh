#!/bin/sh

/bin/echo "====================================="
/bin/echo "[yum] Yellowdog Updater Modified is checking for system update ..."

WARNING_STATUS="N/A"

PATH=/bin:/usr/bin:/sbin:/usr/sbin
export PATH

# Locations of binaries
GREP="/bin/grep"
HOST=`hostname`
MAIL="/bin/mail"
MKTEMP="/bin/mktemp"
YUM="/bin/yum"

# Who to E-mail with new updates
ADMIN=webmaster@sofibox.com

if [ ! -f ${YUM} ];then
        echo "Cannot find ${YUM}"
        exit 1
fi

if [ ! -f ${MKTEMP} ];then
        echo "Cannot find ${MKTEMP}"
        exit 1
fi

if [ ! -f ${MAIL} ];then
        echo "Cannot find ${MAIL}"
        exit 1
fi

if [ ! -f ${GREP} ];then
        echo "Cannot find ${GREP}"
        exit 1
fi

# Dump the yum results to a safe working file
WORK=`${MKTEMP} /tmp/yum.results.XXXXXX`

${YUM} -e0 -d0 check-update > ${WORK}

# If there are updates available, E-mail them
REPORT=`${MKTEMP} /tmp/yum.report.XXXXXX`

if [ -s ${WORK} ];then
 WARNING_STATUS="WARNING"
 echo "[${WARNING_STATUS}] The following updates are available checked for ${HOST} on `date`" > ${REPORT}
else
 WARNING_STATUS="OK"
/bin/echo "[${WARNING_STATUS}] There were no updates available checked for ${HOST} on `date` :" > ${REPORT}
fi

/bin/cat ${WORK} >> ${REPORT}
/bin/cat ${REPORT} | ${MAIL} -s "[yum][${WARNING_STATUS}|N] Yellowdog Updater Modified Check Report @ ${HOST}" ${ADMIN}

# Cleanup temporary files
/bin/rm ${REPORT} ${WORK}
/bin/echo "[yum] Checking status: ${WARNING_STATUS}"
/bin/echo "[yum] Done checking update. Email is set to be sent to ${ADMIN}"
/bin/echo "====================================="